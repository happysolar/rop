library(rOP)

## rOP analysis
# Load the data
# p.table - p-value matrix for the DE analysis in each study
# p.perm - p-value matrix for the permuted data
load("BrainCancer_pval_2sided_with_perm.rdata")

# Calculated the rOP statistics and their p-values
p.rOP <- calc.p.rOP.perm(p.table, p.perm)
colnames(p.rOP) <- paste("rOP", 1:ncol(p.rOP), sep = "")

# Calculate and plot the adjusted number of detection for the selection of r
calc.n.adjust.perm(p.table, p.perm)

# The pathway files in .gmt format for the selection of r
gmtfiles <- c("c2.all.v3.0.symbols.gmt", "c3.all.v3.0.symbols.gmt", "c5.all.v3.0.symbols.gmt")

# Plot the diagnositic plot for the selection of r on the basis of pathway analysis
compare.r.pathway(p.rOP, gmtfiles, 1, 7, 100, cut = 0.05)

# Plot the heatmap for study quality control
plot.hmQC(p.table, p.rOP, r = 5, labRow = NA, margins = c(8, 0.5))


## Compare rOP with other methods
# Load necessary functions which is not included in the "rOP" package because they are only for the paper
source("comp_methods.r")
# Load the gold standard pathways
load("pathway_KEGG_BIOCARTA_REACTOME_GO.rdata")

# Calculated the statistics and their p-values using other methods
p.Fisher <- fisher.pval.perm(p.table, p.perm)
p.Stouffer <- stouffer.pval.perm(p.table, p.perm)

# Perform pathway analysis
p.all <- cbind(p.table, p.rOP, p.Fisher, p.Stouffer)
p.path.all <- gsa.KS.matrix(p.all, pathway = path.cons)


# Vote counting method
vc <- get.vc(p.table)
p.vc <- matrix(vc$p, ncol = 1)
colnames(p.vc) <- "vc"
rownames(p.vc) <- rownames(vc)
p.path.vc <- gsa.KS.matrix(p.vc, pathway = path.cons)
colnames(p.path.vc) <- "VC"
if(all(rownames(p.path.all) == rownames(p.path.vc))) p.path.all.vc <- cbind(p.path.all, p.path.vc)

# Make comparison plot
compare.mth(p.path.all.vc, c(1:4,7), c("rOP5", "rOP1", "rOP7", "p.Fisher", "p.Stouffer", "VC"),
            c("rOP5", "minP", "maxP", "Fisher", "Stouffer", "VC"), main = "")





########################
## One-sided analysis ##
########################

## Analysis
# Load data
load("BrainCancer_pval_1sided_high_with_perm.rdata")
p1 <- p.table
perm1 <- p.perm
load("BrainCancer_pval_1sided_low_with_perm.rdata")
p2 <- p.table
perm2 <- p.perm
rm(p.table, p.perm)

# Calculate one-sided rOP
p.rOP <- calc.p.rOP.onesided.perm(p1, p2, perm1, perm2)
colnames(p.rOP) <- paste("rOP", 1:ncol(p.rOP), sep = "")

# Plot adjusted number of detection
calc.n.adjust.onesided.perm(p1, p2, perm1, perm2)

# Pathway analysis for the selection of r
gmtfiles <- c("c2.all.v3.0.symbols.gmt", "c3.all.v3.0.symbols.gmt", "c5.all.v3.0.symbols.gmt")
compare.r.pathway(p.rOP, gmtfiles, 1, 7, 100, cut = 0.05)

# Heatmap for QC
plot.hmQC.onesided(p1, p2, p.rOP, r = 5, labRow = NA, margins = c(8, 0.5))


## Compare to other methods
source("comp_methods.r")
load("pathway_KEGG_BIOCARTA_REACTOME_GO.rdata")
p.Fisher <- fisher.pval.onesided.perm(p1, p2, perm1, perm2)
p.Stouffer <- stouffer.pval.onesided.perm(p1, p2, perm1, perm2)
p.all <- cbind(p1, p.rOP, p.Fisher, p.Stouffer)
p.path.all <- gsa.KS.matrix(p.all, pathway = path.cons)

# Vote counting
source("vc.r")
vc <- get.vc(p1, p2)
p.vc <- matrix(vc$p, ncol = 1)
colnames(p.vc) <- "vc"
rownames(p.vc) <- rownames(vc)
p.path.vc <- gsa.KS.matrix(p.vc, pathway = path.cons)
colnames(p.path.vc) <- "VC"
if(all(rownames(p.path.all) == rownames(p.path.vc))) p.path.all.vc <- cbind(p.path.all, p.path.vc)

# Make plot
compare.mth(p.path.all.vc, c(1:4,7), c("rOP5", "rOP1", "rOP7", "p.Fisher", "p.Stouffer", "VC"),
            c("rOP5", "minP", "maxP", "Fisher", "Stouffer", "VC"), main = "")
