library(rOP)

## Analysis
# Load data
load("Diabetes_pval_2sided_wo_perm.rdata")
p.table <- p.table[, 16:1]
colnames(p.table) <- paste("s", formatC(1:16, width = 2, flag = "0"), sep = "")

# Calculate rOP
p.rOP <- calc.p.rOP(p.table)
colnames(p.rOP) <- paste("rOP", 1:ncol(p.rOP), sep = "")

# Plot adjusted number of detection
calc.n.adjust(p.table)

# Pathway analysis for the selction of r
gmtfiles <- c("c2.all.v3.0.symbols.gmt", "c3.all.v3.0.symbols.gmt", "c5.all.v3.0.symbols.gmt")
compare.r.pathway(p.rOP, gmtfiles, 1, 16, 100, cut = 0.05)

# Plot heatmap for QC
plot.hmQC(p.table, p.rOP, r = 12, labRow = NA, margins = c(5, 0.5))

## comparison with other methods
source("comp_methods.r")
load("pathway_KEGG_BIOCARTA_REACTOME_GO.rdata")
p.Fisher <- fisher.pval(p.table)
p.Stouffer <- stouffer.pval(p.table)
p.all <- cbind(p.table, p.rOP, p.Fisher, p.Stouffer)
p.path.all <- gsa.KS.matrix(p.all, pathway = path.cons)
save(p.all, p.path.all, file = "pval_path_all.rdata")


# vote counting
source("vc.r")
vc <- get.vc(p.table)
p.vc <- matrix(vc$p, ncol = 1)
colnames(p.vc) <- "vc"
rownames(p.vc) <- rownames(vc)
p.path.vc <- gsa.KS.matrix(p.vc, pathway = path.cons)
colnames(p.path.vc) <- "VC"
if(all(rownames(p.path.all) == rownames(p.path.vc))) p.path.all.vc <- cbind(p.path.all, p.path.vc)

# Make comparison plot
compare.mth(p.path.all.vc, c(1:16), c("rOP12", "rOP1", "rOP16", "p.Fisher", "p.Stouffer", "VC"),
            c("rOP12", "minP", "maxP", "Fisher", "Stouffer", "VC"), main = "")
