library(rOP)

## Analysis
# Prepare data
load("MDD_pval_with_perm.rdata")
p.table <- sapply(Meta9.BIC.p, function(x) x[[1]])
p.perm <- lapply(Meta9.BIC.p, function(x) x[[2]])
rm(Meta9.BIC.p)

# Calculate rOP
p.rOP <- calc.p.rOP.perm(p.table, p.perm)
colnames(p.rOP) <- paste("rOP", 1:ncol(p.rOP), sep = "")

# Plot adjusted number of detection
n.adjust <- calc.n.adjust.perm(p.table, p.perm)

# Plot the pathway analysis result for r selection
gmtfiles <- c("c2.all.v3.0.symbols.gmt", "c3.all.v3.0.symbols.gmt", "c5.all.v3.0.symbols.gmt")
compare.r.pathway(p.rOP, gmtfiles, 1, 9, 100, cut = 0.05)

# Plot heatmap for QC
plot.hmQC(p.table, p.rOP, r = 5, labRow = NA, margins = c(12, 0.5))

# Comparison with other methods
source("comp_methods.r")
load("pathway_KEGG_BIOCARTA_REACTOME_GO.rdata")
p.Fisher <- fisher.pval.perm(p.table, p.perm)
p.Stouffer <- stouffer.pval.perm(p.table, p.perm)
p.all <- cbind(p.table, p.rOP, p.Fisher, p.Stouffer)
p.path.all <- gsa.KS.matrix(p.all, pathway = path.cons)

# vote counting
source("vc.r")
vc <- get.vc(p.table)
p.vc <- matrix(vc$p, ncol = 1)
colnames(p.vc) <- "vc"
rownames(p.vc) <- rownames(vc)
p.path.vc <- gsa.KS.matrix(p.vc, pathway = path.cons)
colnames(p.path.vc) <- "VC"
if(all(rownames(p.path.all) == rownames(p.path.vc))) p.path.all.vc <- cbind(p.path.all, p.path.vc)

# Make comparison plot
compare.mth(p.path.all.vc, c(1:9), c("rOP7", "rOP1", "rOP9", "p.Fisher", "p.Stouffer", "VC"),
            c("rOP7", "minP", "maxP", "Fisher", "Stouffer", "VC"), main = "")
