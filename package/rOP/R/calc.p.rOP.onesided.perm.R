calc.p.rOP.onesided.perm <-
function(p1, p2, perm1, perm2) {
  k <- ncol(p1)
  rOP1 <- t(apply(p1, 1, sort))
  rOP2 <- t(apply(p2, 1, sort))
  rOP.perm1 <- t(apply(sapply(perm1, as.vector), 1, sort))
  rOP.perm2 <- t(apply(sapply(perm2, as.vector), 1, sort))
  rOP <- pmin(rOP1, rOP2)
  rOP.perm <- pmin(rOP.perm1, rOP.perm2)
  colnames(rOP) <- 1:k
  sapply(1:k, function(i) perm.p(rOP[, i], rOP.perm[, i], "low"))
}
