calc.p.rOP <-
function(x) {
    k <- ncol(x)
    rOP <- t(apply(x, 1, sort))
    colnames(rOP) <- 1:k
    sapply(1:k, function(i) pbeta(rOP[,i], i, k - i + 1))
}
