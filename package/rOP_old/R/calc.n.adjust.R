calc.n.adjust <- function(p.table, B = 100, FDR = 0.05) {
    rop <- calc.p.rOP(p.table)
    q <- apply(rop, 2, p.adjust, "BH")
    n <- colSums(q < FDR)
    n.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp1 <- calc.p.rOP(apply(p.table, 2, sample))
        tmp2 <- apply(tmp1, 2, p.adjust, "BH")
        n.base[i, ] <- colSums(tmp2 < FDR)
    }
    mi = min(n, colSums(n.base)/B, n - colSums(n.base)/B)
    ma = max(n, colSums(n.base)/B, n - colSums(n.base)/B)
    plot(c(1,ncol(p.table)), c(mi, ma), type = "n", main = "Adjusted number of detection", xlab = "r", ylab = "number")
    lines(1:ncol(p.table), n, lty = 2, lwd = 3)
    lines(1:ncol(p.table), colSums(n.base)/B, lty = 3, lwd = 3)
    lines(1:ncol(p.table), n - colSums(n.base)/B, lty = 1, lwd = 3)
    legend("topright", c("adjusted", "unadjusted", "baseline"), lty = 1:3, lwd = 3)
    return(list(n = n, n.base = n.base))
}
