compare.r.pathway <-
function(p.rOP, gmtfiles, r1, r2, n.top.path, cut = 0.05) {
    path.pval <- lapply(gmtfiles, function(x) gsa.KS.matrix(p.rOP ,x))
    path.pval.all <- do.call(rbind, path.pval)
    k <- ncol(p.rOP)
    top <- top.path(path.pval.all[, (floor(k/2) + 1):k], n.top.path)
    plotdata <- list(path.pval = path.pval.all, r1 = r1, r2 = r2, top.path = top, cut = cut)
    plotdata2 <- compare.r2(path.pval.all, r1, r2, top, cut)
    return(plotdata)
}
