plot.r.adj.onesided <-
function(p.table, B = 10) {
    rop <- calc.p.rOP.onesided(p.table)
    r <- colSums(outer(apply(rop, 1, which.min), 1:ncol(p.table), "=="))
    r.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp <- calc.p.rOP.onesided(apply(p.table, 2, sample))
        r.base[i, ] <- colSums(outer(apply(tmp, 1, which.min), 1:ncol(p.table), "=="))
    }
    ratio <- r/colMeans(r.base)
    plot(1:ncol(p.table), ratio, type = "l", main = "Ratio of Adaptive r vs Baseline", xlab = "r", ylab = "ratio", lwd = 3)
    return(list(r = r, r.base = r.base))
}
