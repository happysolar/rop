require(GSA)
require(MetaDE)
gsa.KS.matrix <- function(p.value, gmtfile, pathway = NULL, min.length = 5) {
    if(is.null(pathway)) pathway <- GSA.read.gmt(gmtfile)
    k <- ncol(p.value)
    get.KS <- function(pathx) {
        res <- rep(NA, k)
        path <- unlist(strsplit(pathx, " /// "))
        x <- intersect(rownames(p.value), path)
        y <- setdiff(rownames(p.value), path)
        if (length(x) > min.length)
            res <- apply(p.value, 2, function(p) ks.test(p[x], p[y], alternative = "greater")$p)
            #res <- ks.test(x, y, alternative="greater")$p
        return(res)
    }
    pval <- t(sapply(pathway$genesets, get.KS))
    if(nrow(pval) == 1) pval <- t(pval)
    rownames(pval) <- pathway$geneset.names
    return(pval)
}

calc.p.rOP <- function(x) {
    k <- ncol(x)
    rOP <- t(apply(x, 1, sort))
    colnames(rOP) <- 1:k
    sapply(1:k, function(i) pbeta(rOP[,i], i, k - i + 1))
}


calc.p.rOP.perm <- function(x, perm) {
    k <- ncol(x)
    rOP <- t(apply(x, 1, sort))
    rOP.perm <- t(apply(sapply(perm, as.vector), 1, sort))
    colnames(rOP) <- 1:k
    sapply(1:k, function(i) perm.p(rOP[, i], rOP.perm[, i], "low"))
}

calc.p.rOP.onesided.perm <- function(p1, p2, perm1, perm2) {
  k <- ncol(p1)
  rOP1 <- t(apply(p1, 1, sort))
  rOP2 <- t(apply(p2, 1, sort))
  rOP.perm1 <- t(apply(sapply(perm1, as.vector), 1, sort))
  rOP.perm2 <- t(apply(sapply(perm2, as.vector), 1, sort))
  rOP <- pmin(rOP1, rOP2)
  rOP.perm <- pmin(rOP.perm1, rOP.perm2)
  colnames(rOP) <- 1:k
  sapply(1:k, function(i) perm.p(rOP[, i], rOP.perm[, i], "low"))
}


plot.r.adapt <- function(x, ...) {
    r.ad <- apply(x, 1, which.min)
    hist(r.ad, main = "Distribution of adaptive r", xlab = "r", breaks = 1:(ncol(x)+1) - 0.5, ...)
}

plot.r.adj <- function(p.table, B = 10) {
    rop <- calc.p.rOP(p.table)
    r <- colSums(outer(apply(rop, 1, which.min), 1:ncol(p.table), "=="))
    r.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp <- calc.p.rOP(apply(p.table, 2, sample))
        r.base[i, ] <- colSums(outer(apply(tmp, 1, which.min), 1:ncol(p.table), "=="))
    }
    ratio <- r/colMeans(r.base)
    plot(1:ncol(p.table), ratio, type = "l", main = "Ratio of Adaptive r vs Baseline", xlab = "r", ylab = "ratio", lwd = 3)
    return(list(r = r, r.base = r.base))
}

plot.r.diff <- function(p.table, B = 100) {
    rop <- calc.p.rOP(p.table)
    r <- colSums(outer(apply(rop, 1, which.min), 1:ncol(p.table), "=="))
    r.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp <- calc.p.rOP(apply(p.table, 2, sample))
        r.base[i, ] <- colSums(outer(apply(tmp, 1, which.min), 1:ncol(p.table), "=="))
    }
    ratio <- r - colMeans(r.base)
    plot(1:ncol(p.table), ratio, type = "l", main = "Ratio of Adaptive r vs Baseline", xlab = "r", ylab = "diff", lwd = 3)
    return(list(r = r, r.base = r.base))
}

plot.r.adj.onesided <- function(p.table, B = 10) {
    rop <- calc.p.rOP.onesided(p.table)
    r <- colSums(outer(apply(rop, 1, which.min), 1:ncol(p.table), "=="))
    r.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp <- calc.p.rOP.onesided(apply(p.table, 2, sample))
        r.base[i, ] <- colSums(outer(apply(tmp, 1, which.min), 1:ncol(p.table), "=="))
    }
    ratio <- r/colMeans(r.base)
    plot(1:ncol(p.table), ratio, type = "l", main = "Ratio of Adaptive r vs Baseline", xlab = "r", ylab = "ratio", lwd = 3)
    return(list(r = r, r.base = r.base))
}

plot.r.diff.onesided <- function(p.table, B = 10) {
    rop <- calc.p.rOP.onesided(p.table)
    r <- colSums(outer(apply(rop, 1, which.min), 1:ncol(p.table), "=="))
    r.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp <- calc.p.rOP.onesided(apply(p.table, 2, sample))
        r.base[i, ] <- colSums(outer(apply(tmp, 1, which.min), 1:ncol(p.table), "=="))
    }
    ratio <- r - colMeans(r.base)
    plot(1:ncol(p.table), ratio, type = "l", main = "Ratio of Adaptive r vs Baseline", xlab = "r", ylab = "diff", lwd = 3)
    return(list(r = r, r.base = r.base))
}

plot.ndetect <- function(x, FDR = 0.05) {
    q.rOP <- apply(x, 2, p.adjust, "BY")
    n.det <- colSums(q.rOP < FDR)
    k <- length(n.det)
    r <- 1:k
    plot(r, n.det, main = "Number of detected genes for different r")
}

top.path <- function(x, n = 10) {
    ranks <- apply(x, 2, rank)
    sumrank <- rowSums(ranks)
    return(sort(sumrank)[1:n])
}

compare.r2 <- function(path.pval, r1, r2, top.path, cut = 0.05, main = "-log10(p) vs r") {
    if (r1 < r2)
      inc <- 1
    else
      inc <- -1
    p.wilcox <- rep(NA, length(r1:r2))
    for (i in r1:(r2 - inc)) {
        p.wilcox[i - min(r1, r2) + 1] <- wilcox.test(path.pval[names(top.path), i], path.pval[names(top.path), i + inc], alternative = "less", paired = TRUE)$p.value
    }
    sig.wil <- p.wilcox < cut
    sig.wil[r2 - min(r1, r2) + 1] <- FALSE
    plotdata <- NULL
    for (x in r1:r2) {
        plotdata <- rbind(plotdata, cbind(path.pval[names(top.path), x], x, sig.wil[x - min(r1, r2) + 1], p.wilcox[x - min(r1, r2) + 1]))
    }
    colnames(plotdata) <- c("pval", "r", "sig", "p")
    bar.width <- diff(range(-log10(plotdata[, "pval"]))) * 0.05
    bar.top <- max(-log10(plotdata[, "pval"])) + bar.width
    bar.bot <- max(-log10(plotdata[, "pval"])) + bar.width * 0.5
    p.width <- bar.width * 1.5
    p.top <- bar.top + p.width
    p.cent <- bar.top + p.width/2
    print(bar.top)
    plot(c(r1-0.5, r2+0.5), c(min(-log10(plotdata[, "pval"])), p.top), type = "n", main = main, xlab = "r", ylab = "-log10 p-value")
    boxplot(-log10(pval) ~ r, data = plotdata, #col = sig.wil * 2,
            add = TRUE)
    text(0.7, p.cent, "p =", cex = 0.7)
    for(i in r1:(r2 - inc)) {
        xx <- sort(c(i + inc * 0.15, i + inc * 0.85))
        text(mean(xx), p.cent, format(p.wilcox[i - min(r1, r2) + 1], 2, 2, 3), cex = 0.7)
        if (sig.wil[i - min(r1, r2) + 1]) {
            rect(xx[1], bar.bot, xx[2], bar.top, border = "black", col = "black")
        } else {
            rect(xx[1], bar.bot, xx[2], bar.top, border = "black", col = "white")
        }
    }
    return(plotdata)
}

compare.r.pathway <- function(p.rOP, gmtfiles, r1, r2, n.top.path, cut = 0.05) {
    path.pval <- lapply(gmtfiles, function(x) gsa.KS.matrix(p.rOP ,x))
    path.pval.all <- do.call(rbind, path.pval)
    k <- ncol(p.rOP)
    top <- top.path(path.pval.all[, (floor(k/2) + 1):k], n.top.path)
    plotdata <- list(path.pval = path.pval.all, r1 = r1, r2 = r2, top.path = top, cut = cut)
    plotdata2 <- compare.r2(path.pval.all, r1, r2, top, cut)
    return(plotdata)
}

compare.r <- function(p.table, r1, r2, path, gmtfile, cut = 0.05) {
    if (r1 < r2)
      inc <- 1
    else
      inc <- -1
    rank.table <- apply(p.table, 2, rank)
    pathway <- GSA.read.gmt(gmtfile)
    genesets <- pathway$genesets
    names(genesets) <- pathway$geneset.names
    genes <- intersect(unique(unlist(genesets[names(path)])), rownames(p.table))
    p.wilcox <- rep(NA, length(r1:r2))
    for (i in r1:(r2 - inc)) {
        p.wilcox[i - min(r1, r2) + 1] <- wilcox.test(rank.table[genes, i], rank.table[genes, i + inc], alternative = "less")$p.value
    }
    sig.wil <- p.wilcox < cut
    sig.wil[r2 - min(r1, r2) + 1] <- FALSE
    plotdata <- NULL
    for (x in r1:r2) {
        plotdata <- rbind(plotdata, cbind(rank.table[genes, x], x, sig.wil[x - min(r1, r2) + 1]))
    }
    plotdata <- data.frame(plotdata)
    colnames(plotdata) <- c("rank", "r", "sig")
    boxplot(rank ~ r, data = plotdata, col = sig.wil * 2)
}

calc.n.adjust <- function(p.table, B = 100, FDR = 0.05) {
    rop <- calc.p.rOP(p.table)
    q <- apply(rop, 2, p.adjust, "BH")
    n <- colSums(q < FDR)
    n.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp1 <- calc.p.rOP(apply(p.table, 2, sample))
        tmp2 <- apply(tmp1, 2, p.adjust, "BH")
        n.base[i, ] <- colSums(tmp2 < FDR)
    }
    mi = min(n, colSums(n.base)/B, n - colSums(n.base)/B)
    ma = max(n, colSums(n.base)/B, n - colSums(n.base)/B)
    plot(c(1,ncol(p.table)), c(mi, ma), type = "n", main = "Adjusted number of detection", xlab = "r", ylab = "number")
    lines(1:ncol(p.table), n, lty = 2, lwd = 3)
    lines(1:ncol(p.table), colSums(n.base)/B, lty = 3, lwd = 3)
    lines(1:ncol(p.table), n - colSums(n.base)/B, lty = 1, lwd = 3)
    legend("topright", c("adjusted", "unadjusted", "baseline"), lty = 1:3, lwd = 3)
    return(list(n = n, n.base = n.base))
}


calc.n.adjust.perm <- function(p.table, p.perm, B = 10, FDR = 0.05) {
    rop <- calc.p.rOP.perm(p.table, p.perm)
    q <- apply(rop, 2, p.adjust, "BH")
    n <- colSums(q < FDR)
    n.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        shuffle <- replicate(ncol(p.table), sample(nrow(p.table)))
        p.table.B <- matrix(NA, nrow(p.table), ncol(p.table))
        p.perm.B <- list()
        for(j in 1:ncol(p.table)) {
          p.table.B[, j] <- p.table[shuffle[, j] ,j]
          p.perm.B[[j]] <- p.perm[[j]][shuffle[, j], ]
        }
        tmp1 <- calc.p.rOP.perm(p.table.B, p.perm.B)
        tmp2 <- apply(tmp1, 2, p.adjust, "BH")
        n.base[i, ] <- colSums(tmp2 < FDR)
    }
    mi = min(n, colSums(n.base)/B, n - colSums(n.base)/B)
    ma = max(n, colSums(n.base)/B, n - colSums(n.base)/B)
    plot(c(1,ncol(p.table)), c(mi, ma), type = "n", main = "Adjusted number of detection", xlab = "r", ylab = "number")
    lines(1:ncol(p.table), n, lty = 2, lwd = 3)
    lines(1:ncol(p.table), colSums(n.base)/B, lty = 3, lwd = 3)
    lines(1:ncol(p.table), n - colSums(n.base)/B, lty = 1, lwd = 3)
    legend("topright", c("adjusted", "unadjusted", "baseline"), lty = 1:3, lwd = 3)
    return(list(n = n, n.base = n.base))
}

calc.n.adjust.onesided.perm <- function(p1, p2, perm1, perm2, B = 10, FDR = 0.05) {
    rop <- calc.p.rOP.onesided.perm(p1, p2, perm1, perm2)
    q <- apply(rop, 2, p.adjust, "BH")
    n <- colSums(q < FDR)
    n.base <- matrix(NA, B, ncol(p1))
    for(i in 1:B) {
        print(i)
        shuffle <- replicate(ncol(p1), sample(nrow(p1)))
        p1.B <- p2.B <- matrix(NA, nrow(p1), ncol(p1))
        perm1.B <- perm2.B <- list()
        for(j in 1:ncol(p1)) {
          p1.B[, j] <- p1[shuffle[, j] ,j]
          p2.B[, j] <- p2[shuffle[, j], j]
          perm1.B[[j]] <- perm1[[j]][shuffle[, j], ]
          perm2.B[[j]] <- perm2[[j]][shuffle[, j], ]
        }
        tmp1 <- calc.p.rOP.onesided.perm(p1.B, p2.B, perm1.B, perm2.B)
        tmp2 <- apply(tmp1, 2, p.adjust, "BH")
        n.base[i, ] <- colSums(tmp2 < FDR)
    }
    mi = min(n, colSums(n.base)/B, n - colSums(n.base)/B)
    ma = max(n, colSums(n.base)/B, n - colSums(n.base)/B)
    plot(c(1,ncol(p1)), c(mi, ma), type = "n", main = "Adjusted number of detection", xlab = "r", ylab = "number")
    lines(1:ncol(p1), n, lty = 2, lwd = 3)
    lines(1:ncol(p1), colSums(n.base)/B, lty = 3, lwd = 3)
    lines(1:ncol(p1), n - colSums(n.base)/B, lty = 1, lwd = 3)
    legend("topright", c("adjusted", "unadjusted", "baseline"), lty = 1:3, lwd = 3)
    return(list(n = n, n.base = n.base))
}


get.rop.c <- function(one.pval, rth){
    #require(combinat)
    CDF <- function(n,r,z) {
        if (r >= .5*(n+1)) {
            pval <- ifelse(z>=0.5 & z<1,
                         1-sum(sapply((n-r+1):(r-1),function(y)sum(sapply((n-r+1):(n-y),function(x) dmultinom(c(y,x,n-y-x),n,c(1-z,1-z,2*z-1))))))
                         ,2*(1-pbinom(r-1,n,z)))
        } else {
            pval <- ifelse(z>=0 & z<=0.5,
                         1-sum(sapply((0):(r-1),function(y)sum(sapply(0:(r-1),function(x) dmultinom(c(y,x,n-y-x),n,c(z,z,1-2*z)))))),
                         1)
        }
        return(pval)
    }
    K<-length(one.pval)
    QL<-sort(one.pval)[rth]
    QR<-sort(1-one.pval)[rth]
    rop.c<-min(QL,QR)
    return(CDF(K,rth,rop.c))
}

calc.p.rOP.onesided <- function(x) {
    CDF <- function(z, n, r) {
        if (r >= .5*(n+1)) {
            pval <- ifelse(z>=0.5 & z<1,
                         1-sum(sapply((n-r+1):(r-1),function(y)sum(sapply((n-r+1):(n-y),function(x) dmultinom(c(y,x,n-y-x),n,c(1-z,1-z,2*z-1))))))
                         ,2*(1-pbinom(r-1,n,z)))
        } else {
            pval <- ifelse(z>=0 & z<=0.5,
                         1-sum(sapply((0):(r-1),function(y)sum(sapply(0:(r-1),function(x) dmultinom(c(y,x,n-y-x),n,c(z,z,1-2*z)))))),
                         1)
        }
        return(pval)
    }

    rOP.L <- t(apply(x, 1, sort))
    rOP.U <- t(apply(1 - x, 1, sort))
    rOP <- pmin(rOP.L, rOP.U)

    K <- ncol(rOP)

    cdf.v <- function(r, rop) sapply(rop, CDF, n = K, r = r)
    return(sapply(1:ncol(rOP), function(s) cdf.v(s, rOP[,s])))
}

rOP.onesided <- function(x) {
    rOP.L <- t(apply(x, 1, sort))
    rOP.U <- t(apply(1 - x, 1, sort))
    rOP <- pmin(rOP.L, rOP.U)
    return(rOP)
}

calc.n.adjust.onesided <- function(p.table, B = 10, FDR = 0.05) {
    rop <- calc.p.rOP.onesided(p.table)
    q <- apply(rop, 2, p.adjust, "BY")
    n <- colSums(q < FDR)
    n.base <- matrix(NA, B, ncol(p.table))
    for(i in 1:B) {
        print(i)
        tmp1 <- calc.p.rOP.onesided(apply(p.table, 2, sample))
        tmp2 <- apply(tmp1, 2, p.adjust, "BY")
        n.base[i, ] <- colSums(tmp2 < FDR)
    }
    mi = min(n, colSums(n.base)/B, n - colSums(n.base)/B)
    ma = max(n, colSums(n.base)/B, n - colSums(n.base)/B)
    plot(c(1,ncol(p.table)), c(mi, ma), type = "n", main = "Adjusted number of detection", xlab = "r", ylab = "number")
    lines(1:ncol(p.table), n, lty = 2, lwd = 3)
    lines(1:ncol(p.table), colSums(n.base)/B, lty = 3, lwd = 3)
    lines(1:ncol(p.table), n - colSums(n.base)/B, lty = 1, lwd = 3)
    legend("topright", c("adjusted", "unadjusted", "baseline"), lty = 1:3, lwd = 3)
    return(list(n = n, n.base = n.base))
}

plot.hmQC <- function(p.table, p.rop, r, FDR = 0.05, col = c("gray", "black"), ...) {
    sig <- p.adjust(p.rop[, r], "BY") <= 0.05
    p.sig <- p.table[sig, ]
    hmdata <- t(apply(p.sig, 1, rank)) <= r
    mode(hmdata) <- "numeric"
    heatmap(hmdata, Colv = NA, Rowv = NA, col = col, ...)
}

plot.hmQC.onesided.perm <- function(p1, p2, p.rop, r, FDR = 0.05, col = c("gray", "black"), ...) {
    sig <- p.adjust(p.rop[, r], "BH") <= 0.05
    p1.sig <- p1[sig, ]
    p2.sig <- p2[sig, ]
    rOP.L <- t(apply(p1.sig, 1, sort))
    rOP.U <- t(apply(p2.sig, 1, sort))
    direction <- (rOP.L < rOP.U)[, r]
    p.sig.1sided <- p1.sig * direction + p2.sig * (1 - direction)
    hmdata <- t(apply(p.sig.1sided, 1, rank)) <= r
    mode(hmdata) <- "numeric"
    heatmap(hmdata, Colv = NA, Rowv = NA, col = col, ...)
}
