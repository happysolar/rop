load("ptable_2sided_perm.rdata")
source("functions.r")
source("plot_function.r")

p.rOP <- calc.p.rOP.perm(p.table, p.perm)
colnames(p.rOP) <- paste("rOP", 1:ncol(p.rOP), sep = "")

png("n_adjust_perm.png")
n.adjust <- calc.n.adjust.perm(p.table, p.perm)
dev.off()

setEPS()
postscript("braina_perm.eps", width = 5, height = 5)
plot.nadj(n.adjust, main = "")
dev.off()

#png("r_adp_ratio.png")
#plot.r.adj(p.table)
#dev.off()

#png("r_adp_diff.png")
#plot.r.diff(p.table)
#dev.off()

#png("dist_adp_r.png")
#plot.r.adapt(p.rOP)
#dev.off()

gmtfiles <- c("c2.all.v3.0.symbols.gmt", "c3.all.v3.0.symbols.gmt", "c5.all.v3.0.symbols.gmt")

png("path_boxplot_5.png")
plotdata <- compare.r.pathway(p.rOP, gmtfiles, 1, 7, 100, cut = 0.05)
dev.off()

#names(plotdata) <- c("path.pval", "r1", "r2", "top.path", "cut")
plotdata[["main"]] <- ""
postscript("brainb_perm.eps", width = 5, height = 5)
do.call(compare.r2, plotdata)
dev.off()


#png("hm_qc.png")
#plot.hmQC(p.table, p.rOP, r = 5, labRow = NA, margins = c(8, 0.5))
#dev.off()

postscript("brainc_perm.eps", width = 5, height = 5)
plot.hmQC(p.table, p.rOP, r = 5, labRow = NA, margins = c(8, 0.5))
dev.off()

save(plotdata, n.adjust, p.table, p.rOP, file = "plotdata.rdata")
load("plotdata.rdata")

p.rOP2 <- p.rOP

# comp
source("comp_methods.r")
load("pathway_KEGG_BIOCARTA_REACTOME_GO.rdata")
p.Fisher <- fisher.pval.perm(p.table, p.perm)
p.Stouffer <- stouffer.pval.perm(p.table, p.perm)
p.all <- cbind(p.table, p.rOP, p.Fisher, p.Stouffer)
p.path.all <- gsa.KS.matrix(p.all, pathway = path.cons)
save(p.all, p.path.all, file = "pval_path_all.rdata")


## vote counting
source("vc.r")
vc <- get.vc(p.table)
p.vc <- matrix(vc$p, ncol = 1)
colnames(p.vc) <- "vc"
rownames(p.vc) <- rownames(vc)
p.path.vc <- gsa.KS.matrix(p.vc, pathway = path.cons)
colnames(p.path.vc) <- "VC"
if(all(rownames(p.path.all) == rownames(p.path.vc))) p.path.all.vc <- cbind(p.path.all, p.path.vc)
## r=4, 328 genes
## r=5, 83 genes
##




n.fisher <- fisher.n.adj(p.table)
n.stouffer <- stouffer.n.adj(p.table)

# one-sided analysis
load("ptable_1sided_high_perm.rdata")
p1 <- p.table
perm1 <- p.perm
load("ptable_1sided_low_perm.rdata")
p2 <- p.table
perm2 <- p.perm
source("functions.r")

p.rOP <- calc.p.rOP.onesided.perm(p1, p2, perm1, perm2)
colnames(p.rOP) <- paste("rOP", 1:ncol(p.rOP), sep = "")

png("n_adjust_1sided.png")
n.adjust <- calc.n.adjust.onesided.perm(p1, p2, perm1, perm2)
dev.off()

png("r_adp_ratio_1sided.png")
plot.r.adj.onesided(p.table)
dev.off()

png("r_adp_diff_1sided.png")
plot.r.diff.onesided(p.table)
dev.off()

png("dist_adp_r_1sided.png")
plot.r.adapt(p.rOP)
dev.off()

gmtfiles <- c("c2.all.v3.0.symbols.gmt", "c3.all.v3.0.symbols.gmt", "c5.all.v3.0.symbols.gmt")

png("path_boxplot_1sided.png")
plotdata <- compare.r.pathway(p.rOP, gmtfiles, 1, 7, 100, cut = 0.05)
dev.off()

png("hm_qc_1sided.png")
plot.hmQC.onesided(p.table, p.rOP, r = 5, labRow = NA, margins = c(8, 0.5))
dev.off()

save.image(file = "progress_10022011.rdata")
load("20111014.rdata")

# comp
source("comp_methods.r")
load("pathway_KEGG_BIOCARTA_REACTOME_GO.rdata")
p.Fisher <- fisher.pval.onesided.perm(p1, p2, perm1, perm2)
p.Stouffer <- stouffer.pval.onesided.perm(p1, p2, perm1, perm2)
p.all <- cbind(p1, p.rOP, p.Fisher, p.Stouffer)
p.path.all <- gsa.KS.matrix(p.all, pathway = path.cons)
save(p.all, p.path.all, file = "pval_path_all_1sided.rdata")

## vote counting
source("vc.r")
vc <- get.vc(p1, p2)
p.vc <- matrix(vc$p, ncol = 1)
colnames(p.vc) <- "vc"
rownames(p.vc) <- rownames(vc)
p.path.vc <- gsa.KS.matrix(p.vc, pathway = path.cons)
colnames(p.path.vc) <- "VC"
if(all(rownames(p.path.all) == rownames(p.path.vc))) p.path.all.vc <- cbind(p.path.all, p.path.vc)
## r=4, 1431 genes
## r=5, 717 genes
##



## plot
setEPS()
postscript("brain1a_perm.eps", width = 5, height = 5)
plot.nadj(n.adjust, main = "")
dev.off()

plotdata[["main"]] <- ""
postscript("brain1b_perm.eps", width = 5, height = 5)
do.call(compare.r2, plotdata)
dev.off()

postscript("brain1c_perm.eps", width = 5, height = 5)
plot.hmQC.onesided.perm(p1, p2, p.rOP, r = 5, labRow = NA, margins = c(8, 0.5))
dev.off()

##

G2 <- p.adjust(p.rOP2[,5], "BH") < 0.05
G1 <- p.adjust(p.rOP[,5], "BH") < 0.05
sum(G1 & G2)

save.image(file = "20111014.rdata")
