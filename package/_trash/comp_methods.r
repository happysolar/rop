compare.mth <- function(path.pval, ind, plot.idx, label = NULL, ntop=100, cut = 0.05, main = "-log10(p) vs r") {
    if(is.null(label)) label = plot.idx
    nna <- apply(!is.na(path.pval), 1, all)
    path.pval <- path.pval[nna, ]
    ranksum <- rowSums(apply(path.pval[,ind], 2, rank))
    top.path <- which(rank(ranksum, ties.method = "random") <= ntop)
    path.pval <- path.pval[, plot.idx]
    colnames(path.pval) <- label
    ord <- order(colMeans(path.pval[top.path, ]))
    path.pval <- path.pval[,ord]
    r1 <- 1
    r2 <- length(plot.idx)

    
    if (r1 < r2)
      inc <- 1
    else
      inc <- -1
    p.wilcox <- rep(NA, length(r1:r2))
    for (i in r1:(r2 - inc)) {
        p.wilcox[i - min(r1, r2) + 1] <- wilcox.test(path.pval[names(top.path), i], path.pval[names(top.path), i + inc], alternative = "less", paired = TRUE)$p.value
    }
    sig.wil <- p.wilcox < cut
    sig.wil[r2 - min(r1, r2) + 1] <- FALSE
    plotdata <- NULL
    for (x in r1:r2) {
        plotdata <- rbind(plotdata, cbind(path.pval[names(top.path), x], x, sig.wil[x - min(r1, r2) + 1], p.wilcox[x - min(r1, r2) + 1]))
    }
    colnames(plotdata) <- c("pval", "r", "sig", "p")
    bar.width <- diff(range(-log10(plotdata[, "pval"]))) * 0.05
    bar.top <- max(-log10(plotdata[, "pval"])) + bar.width
    bar.bot <- max(-log10(plotdata[, "pval"])) + bar.width * 0.5
    p.width <- bar.width * 1.5
    p.top <- bar.top + p.width
    p.cent <- bar.top + p.width/2
    print(bar.top)
    plot(c(r1-0.5, r2+0.5), c(min(-log10(plotdata[, "pval"])), p.top), type = "n", main = main, xlab = NA, ylab = "-log10 p-value", xaxt = "n")
    axis(1, r1:r2, colnames(path.pval))
    boxplot(-log10(pval) ~ r, data = plotdata, xaxt="n", #col = sig.wil * 2,
            add = TRUE)
    text(0.7, p.cent, "p =", cex = 0.8)
    for(i in r1:(r2 - inc)) {
        xx <- sort(c(i + inc * 0.15, i + inc * 0.85))
        text(mean(xx), p.cent, format(p.wilcox[i - min(r1, r2) + 1], 2, 2, 3), cex = 0.8)
        if (sig.wil[i - min(r1, r2) + 1]) {
            rect(xx[1], bar.bot, xx[2], bar.top, border = "black", col = "black")
        } else {
            rect(xx[1], bar.bot, xx[2], bar.top, border = "black", col = "white")
        }
    }
    return(plotdata)
}


path.trace <- function(p.path, ind, plot.idx, max = 200) {
    nna <- apply(!is.na(p.path), 1, all)
    p.path <- p.path[nna, ]
    ranksum <- rowSums(apply(p.path[, ind], 2, rank))
    ord <- order(ranksum)
    p.ord <- p.path[ord, ]
    p.rank <- t(apply(p.ord, 1, rank))/ncol(p.ord)
    runningsum <- apply(p.rank, 2, cumsum)
    runningmean <- runningsum/(1:nrow(runningsum))
    colnames(runningmean) <- colnames(p.path)
    plot(c(1, max), c(0, 1), type = "n", main = "Running Average", xlab = "n", ylab = "mean Rank")
    print(dim(p.ord))
    for(i in 1:length(plot.idx)) {
        lines(1:max, runningmean[1:max, plot.idx[i]], lty = i, lwd = 3)
    }
    legend("topright", plot.idx, lty = 1:length(plot.idx), lwd = 3)
    comp.ord <- order(runningmean[max, plot.idx])
    meth.ord <- plot.idx[comp.ord]
    p.comp <- p.ord[1:max, meth.ord]
    tests <- vector("list", length(meth.ord) - 1)
    for(i in 1:(length(meth.ord) - 1)) {
        p1 <- p.comp[, i]
        p2 <- p.comp[, i+1]
        tests[[i]] <- wilcox.test(p1, p2, paired = TRUE, alternative = "less")
    }
    return(list(meth.ord = meth.ord, tests = tests))
}

fisher.pval <- function(ptable) {
    k <- ncol(ptable)
    stat <- rowSums(-log(ptable))
    pval <- 1 - pgamma(stat, shape = k)
    return(pval)
}

fisher.pval.perm <- function(ptable, perm) {
    k <- ncol(ptable)
    stat <- rowSums(-log(ptable))
    null <- -log(perm[[1]])
    for(i in 2:k) {
      null <- null - log(perm[[i]])
    }
    pval <- perm.p(stat, null, tail = "high")
    return(pval)
}

fisher.pval.onesided.perm <- function(p1, p2, perm1, perm2) {
    k <- ncol(p1)
    stat1 <- rowSums(-log(p1))
    stat2 <- rowSums(-log(p2))
    null1 <- -log(perm1[[1]])
    null2 <- -log(perm2[[1]])
    for(i in 2:k) {
      null1 <- null1 - log(perm1[[i]])
      null2 <- null2 - log(perm2[[i]])
    }
    stat <- pmax(stat1, stat2)
    null <- pmax(null1, null2)
    pval <- perm.p(stat, null, tail = "high")
    return(pval)
}


fisher.n.adj <- function(ptable, B = 100, FDR = 0.05) {
    p <- fisher.pval(ptable)
    q <- p.adjust(p, "BH")
    n <- sum(q <= FDR)
    p.null <- replicate(B, fisher.pval(apply(ptable, 2, sample)))
    q.null <- apply(p.null, 2, p.adjust, method = "BH")
    n.null <- mean(colSums(q.null <= FDR))
    n.adj <- n - n.null
    return(list(n = n, n.null = n.null, n.adj = n.adj))
}

fisher.pval.onesided <- function(ptable, B = 100000) {
    k <- ncol(ptable)
    null.p <- matrix(runif(B * k), nrow = B, ncol = k)
    null.q <- 1 - null.p
    null.s1 <- rowSums(-log(null.p))
    null.s2 <- rowSums(-log(null.q))
    null.s <- pmax(null.s1, null.s2)
    stat1 <- rowSums(-log(ptable))
    stat2 <- rowSums(-log(1 - ptable))
    stat <- pmax(stat1, stat2)
    pval <- perm.p(stat, null.s, "high")
    return(pval)
}

stouffer.pval <- function(ptable, one.sided = FALSE) {
    k <- ncol(ptable)
    stat <- rowSums(qnorm(ptable))/sqrt(k)
    pval <- pnorm(stat)
    if (one.sided) pval <- pmin(pval, 1 - pval) * 2
    return(pval)
}

stouffer.pval.perm <- function(ptable, perm) {
    k <- ncol(ptable)
    stat <- rowSums(qnorm(ptable))/sqrt(k)
    null <- qnorm(perm[[1]])
    for(i in 2:k) {
      null <- null + qnorm(perm[[i]])
    }
    null <- null/sqrt(k)
    pval <- perm.p(stat, null, "low")
    return(pval)
}

stouffer.pval.onesided.perm <- function(p1, p2, perm1, perm2) {
    k <- ncol(p1)
    stat1 <- rowSums(qnorm(p1))/sqrt(k)
    stat2 <- rowSums(qnorm(p2))/sqrt(k)
    null1 <- qnorm(perm1[[1]])
    null2 <- qnorm(perm2[[1]])
    for(i in 2:k) {
      null1 <- null1 + qnorm(perm1[[i]])
      null2 <- null2 + qnorm(perm2[[i]])
    }
    stat <- pmin(stat1, stat2)
    null <- pmin(null1, null2)
    null <- null/sqrt(k)
    pval <- perm.p(stat, null, "low")
    return(pval)
}

stouffer.n.adj <- function(ptable, B = 100, FDR = 0.05) {
    p <- stouffer.pval(ptable)
    q <- p.adjust(p, "BH")
    n <- sum(q <= FDR)
    p.null <- replicate(B, stouffer.pval(apply(ptable, 2, sample)))
    q.null <- apply(p.null, 2, p.adjust, method = "BH")
    n.null <- mean(colSums(q.null <= FDR))
    n.adj <- n - n.null
    return(list(n = n, n.null = n.null, n.adj = n.adj))
}



#perm.p <- function(stat, perm, tail) {
#    G <- length(stat)
#    B <- length(perm)
#    if (tail == "low") {
#        r <- rank(c(stat, as.vector(perm)), ties.method = "max")[1:G]
#        r2 <- rank(c(stat), ties.method = "max")
#        r3 <- r - r2
#        p <- r3/B
#    }
#    if (tail == "high") {
#        r <- rank(c(stat, as.vector(perm)), ties.method = "min")[1:G]
#        r2 <- rank(stat, ties.method = "max")
#        r3 <- r - r2
#        p <- 1 - r3/B
#    }
#    if (tail == "abs") {
#        r <- rank(c(abs(stat), abs(as.vector(perm))), ties.method = "min")[1:G]
#        r2 <- rank(c(abs(stat)), ties.method = "max")
#        r3 <- r - r2
#        p <- 1 - r3/B
#    }
#    p[p == 0] <- 1e-20
#    p[p == 1] <- 1 - 1e-10
#    return(p)
#}
