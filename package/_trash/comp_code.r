load("pval_path_all.rdata")
source("comp_methods.r")


png("running_path.png")
wil <- path.trace(p.path.all, c(1:4,7), c("rOP5", "rOP1", "rOP7", "p.Fisher", "p.Stouffer"), max = 100)
dev.off()

sink("wil_comp.txt")
cat(wil[[1]], "\n")
cat(sapply(wil[[2]], function(x) x$p.value), "\n")
sink()

p.ind <- p.path.all[,1:7]


load("pval_path_all_1sided.rdata")
source("comp_methods.r")
p.path.all[,1:7] <- p.ind

png("running_path_1sided.png")
wil <- path.trace(p.path.all, c(1:4,7), c("rOP5", "rOP1", "rOP7", "p.Fisher", "p.Stouffer"), max = 100)
dev.off()

sink("wil_comp_1sided.txt")
cat(wil[[1]], "\n")
cat(sapply(wil[[2]], function(x) x$p.value), "\n")
sink()


rOP5.onesided <- p.path.all[, "rOP5"]
load("pval_path_all.rdata")
p.path.all <- cbind(p.path.all, rOP5.onesided)

png("running_path_12sided.png")
wil <- path.trace(p.path.all, c(1:4,7), c("rOP5", "rOP5.onesided", "rOP1", "rOP7", "p.Fisher", "p.Stouffer"), max = 100)
dev.off()

sink("wil_comp_12sided.txt")
cat(wil[[1]], "\n")
cat(sapply(wil[[2]], function(x) x$p.value), "\n")
sink()

## plot
setEPS()
postscript("braincomp_perm_vc.eps", width = 6, height = 5)
compare.mth(p.path.all.vc, c(1:4,7), c("rOP5", "rOP1", "rOP7", "p.Fisher", "p.Stouffer", "VC"),
            c("rOP5", "minP", "maxP", "Fisher", "Stouffer", "VC"), main = "")
dev.off()
