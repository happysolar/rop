require(GSA)
gsa.KS <- function(p.value, gmtfile) {
    pathway <- GSA.read.gmt(gmtfile)
    get.KS <- function(path) {
        res <- NA
        x <- p.value[intersect(names(p.value), path)]
        y <- p.value[setdiff(names(p.value), path)]
        if (length(x) > 0)
          res <- ks.test(x, y, alternative="greater")$p
        return(res)
    }
    pval <- sapply(pathway$genesets, get.KS)
    names(pval) <- pathway$geneset.names
    qval <- p.adjust(pval)
    return(list(pval=pval, qval=qval, desc=pathway$geneset.descriptions))
}
