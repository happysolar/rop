plot.nadj <- function(x, main = "Adjusted number of detection") {
    n <- x$n
    k <- length(n)
    base <- colMeans(x$n.base)
    adj <- n - base
    miny <- min(n, base, adj)
    maxy <- max(n, base, adj)
    plot(c(1,k), c(miny, maxy), type = "n", main = main, xlab = "r", ylab = "number")
    lines(1:k, adj, lwd = 3, lty = 1)
    lines(1:k, n, lwd = 3, lty = 2)
    lines(1:k, base, lwd = 3, lty = 3)
    legend("topright", c("adjusted", "unadjusted", "baseline"), lwd = 3, lty = 1:3)
}
