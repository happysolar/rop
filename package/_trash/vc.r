get.vc <- function(p, cut = 0.05, pi = 0.5) {
  k <- ncol(p)
  q <- apply(p, 2, p.adjust, "BH")
  count <- rowSums(q <= cut)
  p.vc <- pbinom(k - count, k, 1 - pi)
  q.vc <- p.adjust(p.vc, "BH")
  return(data.frame(vc = count, p = p.vc, fdr = q.vc))
}

get.vc.onesided <- function(p1, p2, cut = 0.05, pi = 0.5) {
  k <- ncol(p1)
  q1 <- apply(p1, 2, p.adjust, "BH")
  q2 <- apply(p2, 2, p.adjust, "BH")
  count1 <- rowSums(q1 <= cut)
  count2 <- rowSums(q2 <= cut)
  count <- pmax(count1, count2)
  p.vc <- pbinom(k - count, k, 1 - pi)
  q.vc <- p.adjust(p.vc, "BH")
  return(data.frame(vc = count, p = p.vc, fdr = q.vc))
}
